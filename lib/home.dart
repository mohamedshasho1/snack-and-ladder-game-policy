import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'componants/dialog_choose_game.dart';

class Home extends StatefulWidget {
  static const String id = 'home';
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double _dragValue = 0;
  @override
  Widget build(BuildContext context) {
    var height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    var width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white70,
        body: Column(
          children: [
            SizedBox(height: height * 0.05),
            Text(
              'Welcome To Game',
              style: TextStyle(
                decoration: TextDecoration.underline,
                color: Colors.lightBlue,
                fontSize: 25,
              ),
            ),
            SizedBox(height: height * 0.05),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'images/snake.svg',
                  height: height * 0.1,
                ),
                SvgPicture.asset(
                  'images/ladder.svg',
                  height: height * 0.1,
                ),
              ],
            ),
            SizedBox(height: height * 0.1),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                elevation: MaterialStateProperty.all(5),
              ),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (ctx) {
                      return DialogChooseGame();
                    });
              },
              child: Text("Play Now!"),
            ),
            Spacer(),
            Image.asset('images/dicebackground.png'),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
