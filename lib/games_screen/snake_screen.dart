import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ladder_snake/componants/build_alertDialog.dart';
import 'package:ladder_snake/home.dart';
import 'package:ladder_snake/models/count_Player.dart';
import 'package:ladder_snake/models/ladder_position.dart';

class SnakeGame extends StatefulWidget {
  static const String id = 'snakeGame';
  final countPlayer countplayer;

  const SnakeGame({this.countplayer});
  @override
  _SnakeGameState createState() => _SnakeGameState();
}

class _SnakeGameState extends State<SnakeGame> with TickerProviderStateMixin {
  bool isMe = true;
  int _currentPos = 1;
  int _currentPos2 = 1;
  int diceNum = 1;
  int diceNum2 = 1;
  int _scorePlayer1 = 0;
  int _scorePlayer2 = 0;
  Animation<double> _animation;
  AnimationController _controller;
  Animation<double> _animation2;
  AnimationController _controller2;
  LadderPosition playerPosition1 = LadderPosition();
  LadderPosition playerPosition2 = LadderPosition();
  AudioCache audioPlayer = AudioCache();
  void clearData() {
    setState(() {
      isMe = true;
      _currentPos = 1;
      _currentPos2 = 1;
      diceNum = 1;
      diceNum2 = 1;
    });
  }

  ///start in 1
  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(_controller);
    _controller.forward();

    _controller2 =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation2 = Tween<double>(begin: 0.0, end: 1.0).animate(_controller2);
    _controller2.forward();
  }

  @override
  void dispose() {
    super.dispose();
    audioPlayer.clearAll();
    _controller.dispose();
    _controller2.dispose();
  }

  Future<bool> _backPress() async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => BuildAlert(
              title: 'Do you want leave?!',
              onPress: () {
                Navigator.of(context).pop(false);
              },
              btnTitle: 'NO',
              onExit: () {
                Navigator.of(context).pop(true);
                Navigator.pop(context);
              },
            ));
  }

  @override
  Widget build(BuildContext context) {
    // countPlayer _countplayer = ModalRoute.of(context).settings.arguments;

    var height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    var width = MediaQuery.of(context).size.width;

    double dx1 = playerPosition1.getPos(_currentPos).dx;
    double dy1 = playerPosition1.getPos(_currentPos).dy;
    double dx2 = playerPosition2.getPos(_currentPos2).dx;
    double dy2 = playerPosition2.getPos(_currentPos2).dy;

    return WillPopScope(
      onWillPop: _backPress,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.tealAccent.shade200,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: height * 0.1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    widget.countplayer == countPlayer.two
                        ? const Text('Player 1')
                        : const SizedBox(),
                    SizedBox(width: width * 0.02),
                    Text(
                      '$_scorePlayer1 : $_scorePlayer2',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(width: width * 0.02),
                    widget.countplayer == countPlayer.two
                        ? const Text('Player 2')
                        : const SizedBox(),
                  ],
                ),
              ),
              Container(
                height: height * 0.15,
                color: Colors.teal.shade100,
                padding: const EdgeInsets.all(10),
                child: InkWell(
                  onTap: !isMe && (_currentPos != 100 && _currentPos2 != 100)
                      ? () => onClickPlayer2()
                      : null,
                  child: ScaleTransition(
                    scale: _animation2,
                    child: SvgPicture.asset(
                      'images/dice$diceNum2.svg',
                    ),
                  ),
                ),
              ),
              Container(
                height: height * 0.6,
                child: LayoutBuilder(
                  builder: (ctx, constraints) {
                    return Stack(
                      children: [
                        Image.asset(
                          'images/snake.jpg',
                          fit: BoxFit.fill,
                          width: double.infinity,
                          height: double.infinity,
                        ),
                        AnimatedPositioned(
                          duration: Duration(milliseconds: 500),
                          child: SvgPicture.asset(
                            'images/pawn_blue.svg',
                            height: constraints.maxHeight * 0.1,
                            width: constraints.maxWidth * 0.1,
                          ),
                          bottom: 0 + constraints.maxHeight * dy2,
                          left: 0 + width * dx2,
                        ),
                        AnimatedPositioned(
                          duration: Duration(milliseconds: 500),
                          child: SvgPicture.asset(
                            'images/pawn_red.svg',
                            height: constraints.maxHeight * 0.1,
                            width: constraints.maxWidth * 0.1,
                          ),
                          bottom: 0 + constraints.maxHeight * dy1,
                          left: 0 + width * (dx1 - 0.01),
                        ),
                      ],
                    );
                  },
                ),
              ),
              Container(
                height: height * 0.15,
                color: Colors.teal.shade100,
                padding: const EdgeInsets.all(10),
                child: InkWell(
                  onTap: isMe && (_currentPos != 100 && _currentPos2 != 100)
                      ? () => onClickPlayer1()
                      : null,
                  child: ScaleTransition(
                    scale: _animation,
                    child: SvgPicture.asset(
                      'images/dice$diceNum.svg',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> onClickPlayer2() async {
    if (_currentPos2 == 100) return;

    isMe = !isMe;
    await audioPlayer.play('rollSound.mp3');
    await _controller2.reverse();
    setState(() {
      diceNum2 = Random().nextInt(6) + 1;
    });
    await _controller2.forward();
    setState(() {
      if ((playerPosition2.currentPos + diceNum2) <= 100) {
        _currentPos2 = playerPosition2.currentPos + diceNum2;
        if (_currentPos2 == 100) {
          score();
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (ctx) => BuildAlert(
                    title: widget.countplayer == countPlayer.one
                        ? 'Game Over!'
                        : 'Player 2 Won!',
                    onPress: () {
                      clearData();
                      Navigator.pop(ctx);
                    },
                    btnTitle: 'One more',
                    onExit: () => Navigator.pushNamedAndRemoveUntil(
                        context, Home.id, (route) => false),
                  ));
        }
      } else {
        _currentPos2 = 100;
        score();
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (ctx) => BuildAlert(
                  title: widget.countplayer == countPlayer.one
                      ? 'Game Over!'
                      : 'Player 2 Won!',
                  btnTitle: 'One more',
                  onPress: () {
                    clearData();
                    Navigator.pop(ctx);
                  },
                  onExit: () => Navigator.pushNamedAndRemoveUntil(
                      context, Home.id, (route) => false),
                ));
      }
    });
  }

  Future<void> onClickPlayer1() async {
    isMe = !isMe;
    await _controller.reverse();
    await audioPlayer.play('rollSound.mp3');
    setState(() {
      diceNum = Random().nextInt(6) + 1;
    });
    await _controller.forward();
    setState(() {
      if ((playerPosition1.currentPos + diceNum) <= 100) {
        _currentPos = playerPosition1.currentPos + diceNum;
        if (_currentPos == 100) {
          score();
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (ctx) => BuildAlert(
                    title: widget.countplayer == countPlayer.one
                        ? 'You Won!'
                        : 'Player 1 Won!',
                    onPress: () {
                      clearData();
                      Navigator.pop(ctx);
                    },
                    btnTitle: 'One more',
                    onExit: () => Navigator.pushNamedAndRemoveUntil(
                        context, Home.id, (route) => false),
                  ));
        }
      } else {
        _currentPos = 100;
        score();
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (ctx) => BuildAlert(
                  title: widget.countplayer == countPlayer.one
                      ? 'You Won!'
                      : 'Player 1 Won!',
                  onPress: () {
                    clearData();
                    Navigator.pop(ctx);
                  },
                  btnTitle: 'One more',
                  onExit: () => Navigator.pushNamedAndRemoveUntil(
                      context, Home.id, (route) => false),
                ));
      }
    });
    if (widget.countplayer == countPlayer.one) {
      if (_currentPos != 100) await onClickPlayer2();
    } else {}
  }

  void score() {
    setState(() {
      if (_currentPos == 100)
        _scorePlayer1++;
      else
        _scorePlayer2++;
    });
  }
}
