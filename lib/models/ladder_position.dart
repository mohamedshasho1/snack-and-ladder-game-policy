class Pos {
  double dx = 0.0;
  double dy = 0.0;
}

class LadderPosition {
  int currentPos = 1;
  Pos getPos(int i) {
    Pos pos = Pos();
    switch (i) {
      case 1:
        {
          pos.dx = 0.0;
          pos.dy = 0.0;
          currentPos = 1;
          return pos;
        }
      case 2:
        {
          pos.dx = 0.1;
          pos.dy = 0.0;
          currentPos = 2;
          return pos;
        }
      case 3:
        {
          pos.dx = 0.0;
          pos.dy = 0.5;
          currentPos = 51;
          return pos;
        }
      case 4:
        {
          pos.dx = 0.3;
          pos.dy = 0.0;
          currentPos = 4;
          return pos;
        }
      case 5:
        {
          pos.dx = 0.4;
          pos.dy = 0.0;
          currentPos = 5;
          return pos;
        }
      case 6:
        {
          pos.dx = 0.6;
          pos.dy = 0.2;
          currentPos = 27;
          return pos;
        }
      case 7:
        {
          pos.dx = 0.6;
          pos.dy = 0.0;
          currentPos = 7;
          return pos;
        }
      case 8:
        {
          pos.dx = 0.7;
          pos.dy = 0.0;
          currentPos = 8;
          return pos;
        }
      case 9:
        {
          pos.dx = 0.8;
          pos.dy = 0.0;
          currentPos = 9;
          return pos;
        }
      case 10:
        {
          pos.dx = 0.9;
          pos.dy = 0.0;
          currentPos = 10;
          return pos;
        }
      case 11:
        {
          pos.dx = 0.0;
          pos.dy = 0.1;
          currentPos = 11;
          return pos;
        }
      case 12:
        {
          pos.dx = 0.1;
          pos.dy = 0.1;
          currentPos = 12;
          return pos;
        }
      case 13:
        {
          pos.dx = 0.2;
          pos.dy = 0.1;
          currentPos = 13;
          return pos;
        }
      case 14:
        {
          pos.dx = 0.3;
          pos.dy = 0.1;
          currentPos = 14;
          return pos;
        }
      case 15:
        {
          pos.dx = 0.4;
          pos.dy = 0.1;
          currentPos = 15;
          return pos;
        }
      case 16:
        {
          pos.dx = 0.5;
          pos.dy = 0.1;
          currentPos = 16;
          return pos;
        }
      case 17:
        {
          pos.dx = 0.6;
          pos.dy = 0.1;
          currentPos = 17;
          return pos;
        }
      case 18:
        {
          pos.dx = 0.7;
          pos.dy = 0.1;
          currentPos = 18;
          return pos;
        }
      case 19:
        {
          pos.dx = 0.8;
          pos.dy = 0.1;
          currentPos = 19;
          return pos;
        }
      case 20:
        {
          pos.dx = 0.9;
          pos.dy = 0.6;
          currentPos = 70;
          return pos;
        }
      case 21:
        {
          pos.dx = 0.0;
          pos.dy = 0.2;
          currentPos = 21;
          return pos;
        }
      case 22:
        {
          pos.dx = 0.1;
          pos.dy = 0.2;
          currentPos = 22;
          return pos;
        }
      case 23:
        {
          pos.dx = 0.2;
          pos.dy = 0.2;
          currentPos = 23;
          return pos;
        }
      case 24:
        {
          pos.dx = 0.3;
          pos.dy = 0.2;
          currentPos = 24;
          return pos;
        }
      case 25:
        {
          pos.dx = 0.4;
          pos.dy = 0.0;
          currentPos = 5;
          return pos;
        }
      case 26:
        {
          pos.dx = 0.5;
          pos.dy = 0.2;
          currentPos = 26;
          return pos;
        }
      case 27:
        {
          pos.dx = 0.6;
          pos.dy = 0.2;
          currentPos = 27;
          return pos;
        }
      case 28:
        {
          pos.dx = 0.7;
          pos.dy = 0.2;
          currentPos = 28;
          return pos;
        }
      case 29:
        {
          pos.dx = 0.8;
          pos.dy = 0.2;
          currentPos = 29;
          return pos;
        }
      case 30:
        {
          pos.dx = 0.9;
          pos.dy = 0.2;
          currentPos = 30;
          return pos;
        }
      case 31:
        {
          pos.dx = 0.0;
          pos.dy = 0.3;
          currentPos = 31;
          return pos;
        }
      case 32:
        {
          pos.dx = 0.1;
          pos.dy = 0.3;
          currentPos = 32;
          return pos;
        }
      case 33:
        {
          pos.dx = 0.2;
          pos.dy = 0.3;
          currentPos = 33;
          return pos;
        }
      case 34:
        {
          pos.dx = 0.0;
          pos.dy = 0.0;
          currentPos = 1;
          return pos;
        }
      case 35:
        {
          pos.dx = 0.4;
          pos.dy = 0.3;
          currentPos = 35;
          return pos;
        }
      case 36:
        {
          pos.dx = 0.4;
          pos.dy = 0.5;
          currentPos = 55;
          return pos;
        }
      case 37:
        {
          pos.dx = 0.6;
          pos.dy = 0.3;
          currentPos = 37;
          return pos;
        }
      case 38:
        {
          pos.dx = 0.7;
          pos.dy = 0.3;
          currentPos = 38;
          return pos;
        }
      case 39:
        {
          pos.dx = 0.8;
          pos.dy = 0.3;
          currentPos = 39;
          return pos;
        }
      case 40:
        {
          pos.dx = 0.9;
          pos.dy = 0.3;
          currentPos = 40;
          return pos;
        }
      case 41:
        {
          pos.dx = 0.00;
          pos.dy = 0.4;
          currentPos = 41;
          return pos;
        }
      case 42:
        {
          pos.dx = 0.1;
          pos.dy = 0.4;
          currentPos = 42;
          return pos;
        }
      case 43:
        {
          pos.dx = 0.2;
          pos.dy = 0.4;
          currentPos = 43;
          return pos;
        }
      case 44:
        {
          pos.dx = 0.3;
          pos.dy = 0.4;
          currentPos = 44;
          return pos;
        }
      case 45:
        {
          pos.dx = 0.4;
          pos.dy = 0.4;
          currentPos = 45;
          return pos;
        }
      case 46:
        {
          pos.dx = 0.5;
          pos.dy = 0.4;
          currentPos = 46;
          return pos;
        }
      case 47:
        {
          pos.dx = 0.8;
          pos.dy = 0.1;
          currentPos = 19;
          return pos;
        }
      case 48:
        {
          pos.dx = 0.7;
          pos.dy = 0.4;
          currentPos = 48;
          return pos;
        }
      case 49:
        {
          pos.dx = 0.8;
          pos.dy = 0.4;
          currentPos = 49;
          return pos;
        }
      case 50:
        {
          pos.dx = 0.9;
          pos.dy = 0.4;
          currentPos = 50;
          return pos;
        }
      case 51:
        {
          pos.dx = 0.0;
          pos.dy = 0.5;
          currentPos = 51;
          return pos;
        }
      case 52:
        {
          pos.dx = 0.1;
          pos.dy = 0.5;
          currentPos = 52;
          return pos;
        }
      case 53:
        {
          pos.dx = 0.2;
          pos.dy = 0.5;
          currentPos = 53;
          return pos;
        }
      case 54:
        {
          pos.dx = 0.3;
          pos.dy = 0.5;
          currentPos = 54;
          return pos;
        }
      case 55:
        {
          pos.dx = 0.4;
          pos.dy = 0.5;
          currentPos = 55;
          return pos;
        }
      case 56:
        {
          pos.dx = 0.5;
          pos.dy = 0.5;
          currentPos = 56;
          return pos;
        }
      case 57:
        {
          pos.dx = 0.6;
          pos.dy = 0.5;
          currentPos = 57;
          return pos;
        }
      case 58:
        {
          pos.dx = 0.7;
          pos.dy = 0.5;
          currentPos = 58;
          return pos;
        }
      case 59:
        {
          pos.dx = 0.8;
          pos.dy = 0.5;
          currentPos = 59;
          return pos;
        }
      case 60:
        {
          pos.dx = 0.9;
          pos.dy = 0.5;
          currentPos = 60;
          return pos;
        }
      case 61:
        {
          pos.dx = 0.0;
          pos.dy = 0.6;
          currentPos = 61;
          return pos;
        }
      case 62:
        {
          pos.dx = 0.1;
          pos.dy = 0.6;
          currentPos = 62;
          return pos;
        }
      case 63:
        {
          pos.dx = 0.4;
          pos.dy = 0.9;
          currentPos = 95;
          return pos;
        }
      case 64:
        {
          pos.dx = 0.3;
          pos.dy = 0.6;
          currentPos = 64;
          return pos;
        }
      case 65:
        {
          pos.dx = 0.1;
          pos.dy = 0.5;
          currentPos = 52;
          return pos;
        }
      case 66:
        {
          pos.dx = 0.5;
          pos.dy = 0.6;
          currentPos = 66;
          return pos;
        }
      case 67:
        {
          pos.dx = 0.6;
          pos.dy = 0.6;
          currentPos = 67;
          return pos;
        }
      case 68:
        {
          pos.dx = 0.7;
          pos.dy = 0.9;
          currentPos = 98;
          return pos;
        }
      case 69:
        {
          pos.dx = 0.8;
          pos.dy = 0.6;
          currentPos = 69;
          return pos;
        }
      case 70:
        {
          pos.dx = 0.9;
          pos.dy = 0.6;
          currentPos = 70;
          return pos;
        }
      case 71:
        {
          pos.dx = 0.0;
          pos.dy = 0.7;
          currentPos = 71;
          return pos;
        }
      case 72:
        {
          pos.dx = 0.1;
          pos.dy = 0.7;
          currentPos = 72;
          return pos;
        }
      case 73:
        {
          pos.dx = 0.2;
          pos.dy = 0.7;
          currentPos = 73;
          return pos;
        }
      case 74:
        {
          pos.dx = 0.3;
          pos.dy = 0.7;
          currentPos = 74;
          return pos;
        }
      case 75:
        {
          pos.dx = 0.4;
          pos.dy = 0.7;
          currentPos = 75;
          return pos;
        }
      case 76:
        {
          pos.dx = 0.5;
          pos.dy = 0.7;
          currentPos = 76;
          return pos;
        }
      case 77:
        {
          pos.dx = 0.6;
          pos.dy = 0.7;
          currentPos = 77;
          return pos;
        }
      case 78:
        {
          pos.dx = 0.7;
          pos.dy = 0.7;
          currentPos = 78;
          return pos;
        }
      case 79:
        {
          pos.dx = 0.8;
          pos.dy = 0.7;
          currentPos = 79;
          return pos;
        }
      case 80:
        {
          pos.dx = 0.9;
          pos.dy = 0.7;
          currentPos = 80;
          return pos;
        }
      case 81:
        {
          pos.dx = 0.0;
          pos.dy = 0.8;
          currentPos = 81;
          return pos;
        }
      case 82:
        {
          pos.dx = 0.1;
          pos.dy = 0.8;
          currentPos = 82;
          return pos;
        }
      case 83:
        {
          pos.dx = 0.2;
          pos.dy = 0.8;
          currentPos = 83;
          return pos;
        }
      case 84:
        {
          pos.dx = 0.3;
          pos.dy = 0.8;
          currentPos = 84;
          return pos;
        }
      case 85:
        {
          pos.dx = 0.4;
          pos.dy = 0.8;
          currentPos = 85;
          return pos;
        }
      case 86:
        {
          pos.dx = 0.5;
          pos.dy = 0.8;
          currentPos = 86;
          return pos;
        }
      case 87:
        {
          pos.dx = 0.6;
          pos.dy = 0.5;
          currentPos = 57;
          return pos;
        }
      case 88:
        {
          pos.dx = 0.7;
          pos.dy = 0.8;
          currentPos = 88;
          return pos;
        }
      case 89:
        {
          pos.dx = 0.8;
          pos.dy = 0.8;
          currentPos = 89;
          return pos;
        }
      case 90:
        {
          pos.dx = 0.9;
          pos.dy = 0.8;
          currentPos = 90;
          return pos;
        }
      case 91:
        {
          pos.dx = 0.0;
          pos.dy = 0.6;
          currentPos = 61;
          return pos;
        }
      case 92:
        {
          pos.dx = 0.1;
          pos.dy = 0.9;
          currentPos = 92;
          return pos;
        }
      case 93:
        {
          pos.dx = 0.2;
          pos.dy = 0.9;
          currentPos = 93;
          return pos;
        }
      case 94:
        {
          pos.dx = 0.3;
          pos.dy = 0.9;
          currentPos = 94;
          return pos;
        }
      case 95:
        {
          pos.dx = 0.4;
          pos.dy = 0.9;
          currentPos = 95;
          return pos;
        }
      case 96:
        {
          pos.dx = 0.5;
          pos.dy = 0.9;
          currentPos = 96;
          return pos;
        }
      case 97:
        {
          pos.dx = 0.6;
          pos.dy = 0.9;
          currentPos = 97;
          return pos;
        }
      case 98:
        {
          pos.dx = 0.7;
          pos.dy = 0.9;
          currentPos = 98;
          return pos;
        }
      case 99:
        {
          pos.dx = 0.8;
          pos.dy = 0.6;
          currentPos = 69;
          return pos;
        }
      case 100:
        {
          pos.dx = 0.9;
          pos.dy = 0.9;
          currentPos = 100;
          return pos;
        }
    }
  }
}
