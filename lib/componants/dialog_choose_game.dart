import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ladder_snake/games_screen/snake_screen.dart';
import 'package:ladder_snake/models/count_Player.dart';

class DialogChooseGame extends StatefulWidget {
  @override
  _DialogChooseGameState createState() => _DialogChooseGameState();
}

class _DialogChooseGameState extends State<DialogChooseGame> {
  countPlayer _value = countPlayer.one;
  @override
  Widget build(BuildContext context) {
    void _change(val) {
      setState(() {
        _value = val;
      });
    }

    return AlertDialog(
      backgroundColor: Colors.lightBlueAccent.shade100,
      elevation: 4,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      title: Row(
        children: [
          Text('Choose one!'),
          Spacer(),
          Container(
            transform: Matrix4.translationValues(10, -15, 0),
            child: InkWell(
              child: Icon(Icons.close),
              onTap: () => Navigator.pop(context),
            ),
          ),
        ],
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Divider(),
          Row(
            children: [
              Radio<countPlayer>(
                value: countPlayer.one,
                groupValue: _value,
                onChanged: _change,
              ),
              Text('One'),
              Radio<countPlayer>(
                value: countPlayer.two,
                groupValue: _value,
                onChanged: _change,
              ),
              Text('Two'),
            ],
          ),
        ],
      ),
      actions: [
        ElevatedButton(
          onPressed: () async {
            await Navigator.push(context, MaterialPageRoute(
              builder: (ctx) {
                return SnakeGame(countplayer: _value);
              },
            ));
            Navigator.pop(context);
          },
          child: Text('start'),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.black45),
            elevation: MaterialStateProperty.all(5),
          ),
        ),
      ],
    );
  }
}
