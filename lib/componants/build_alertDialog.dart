import 'package:flutter/material.dart';
import 'package:ladder_snake/home.dart';

class BuildAlert extends StatelessWidget {
  const BuildAlert(
      {@required this.title,
      @required this.onPress,
      @required this.onExit,
      @required this.btnTitle});
  final String title;
  final String btnTitle;
  final Function onPress;
  final Function onExit;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.lightBlueAccent.shade100,
      elevation: 4,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      title: Text(title),
      actions: [
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.teal.shade800),
          ),
          onPressed: onExit,
          child: Text('Exit'),
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.teal.shade800),
          ),
          onPressed: onPress,
          child: Text(btnTitle),
        ),
      ],
    );
  }
}
